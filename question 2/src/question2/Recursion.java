package question2;

public class Recursion {
	public static int recursiveCount(int[]numbers,int n) {
		if(n<numbers.length) {//prevents going out of bounds 
	            if (numbers[n] < 10 && n % 2 == 0) {
	                return recursiveCount(numbers, n + 1) + 1;//counts how many numbers
	            } else {
	                return recursiveCount(numbers, n + 1);//continues with other position
	            }
		}
		return 0;
	}
}

package frontend;

import backend.gameLogics;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class UIapp extends Application{
	UiHelper ui;
	gameLogics g = new gameLogics();
	public static void main(String[] args) {
        launch(args);
    }

	@Override
	public void start(Stage stage) throws Exception {
		Group root = new Group();
		
		VBox v1 = new VBox();
		Label l1 = new Label("Dice Bet!");
		Label l2 = new Label("Enter the amount of money to bet");
		TextField input = new TextField("");
		v1.getChildren().addAll(l1,l2,input);
		
		HBox  h1 = new HBox();
		Button betEven = new Button("Even");
		
		
		Button betOdd = new Button("Odd");
		
		Label money = new Label();
		Label message = new Label();
		
		ui=new UiHelper(g,message,money,input,"even");
		betEven.setOnAction(ui);
		
		ui=new UiHelper(g,message,money,input,"odd");
		betOdd.setOnAction(ui);
		
		h1.getChildren().addAll(betEven,betOdd,money, message);
		
		
		
		VBox box = new VBox();
		box.getChildren().addAll(v1,h1);
		 
		 
		 
		 
		 
		 root.getChildren().add(box);
		 		 
		 
	     Scene scene = new Scene(root, 500, 300);
	     
	     
	     stage.setScene(scene);
	     stage.setTitle("Question 3");
	     stage.show();
	}

}

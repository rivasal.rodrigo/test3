package frontend;

import backend.gameLogics;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class UiHelper implements EventHandler<ActionEvent> {
	
	private String handler;
    private gameLogics dice;
    private Label message;
    private Label money;
    private TextField input;
    
    public UiHelper(gameLogics dice, Label message, Label money, TextField input,String handler) {
        this.handler = handler;
        this.dice = dice;
        this.message = message;
        this.money = money;
        this.input = input;
    }

    @Override
    public void handle(ActionEvent event) {
        if (this.handler.equals("even")) {
            this.message.setText(dice.playRound(this.handler, Integer.parseInt(input.getText())));
            this.money.setText("Money Available: " + dice.getMoney());
        }
        if (this.handler.equals("odd")) {
            this.message.setText(dice.playRound(this.handler, Integer.parseInt(input.getText())));
            this.money.setText("Money Available: " + dice.getMoney());
        }
        
    }
}

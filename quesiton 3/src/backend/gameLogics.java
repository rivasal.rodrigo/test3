package backend;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.util.Random;

public class gameLogics{
	Random ran = new Random();
	int ranNum;
	private int money =250;
	
		public gameLogics() {
		money = 250;
		}			
		
		public String playRound(String choice, int bet) {
			ranNum = ran.nextInt(7);
			
			if(bet<=money) {
				if(choice.equals("even")&& ranNum%2==0) {
					money=money+bet;
					return "You won the number is Even " +ranNum;
				}
				if(choice.equals("odd")&& ranNum%2==1) {
					money=money+bet;
					return "You won the number is Odd "+ ranNum;
				}
				money = money-bet;
				return "You lost " + bet;
			}
			return "Not enough money";
		}
		
		public int getMoney() {
			return money;
		}
		
	
}
